const path = require("path");
const loaders = require("./loaders");
const plugins = require("./plugins");

module.exports = {
  entry: ["./src/js/app.jsx"],
  module: {
    rules: [
      loaders.CSSLoader,
      loaders.JSLoader
      // loaders.ESLintLoader
    ]
  },
  output: {
    path: path.resolve(__dirname, "../dist"),
    filename: "js/[name].bundle.js",
    publicPath: "/"
  },
  resolve: {
    extensions: [" ", ".js", ".jsx"]
  },
  devServer: {
    historyApiFallback: true
  },
  plugins: [
    plugins.HtmlWebpackPlugin,
    plugins.StyleLintPlugin,
    plugins.ExtractTextPlugin
  ]
};
