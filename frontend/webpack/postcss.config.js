module.exports = {
  plugins: {
    "postcss-import": {},
    "@csstools/postcss-sass": {},
    "postcss-css-variables": {},
    "postcss-preset-env": {
      browsers: "last 2 versions"
    },
    cssnano: {}
  }
};
