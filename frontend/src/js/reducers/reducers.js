import {
  DEFAULT_TEST,
  GET_POSTS,
  ITEMS_HAS_ERRORED,
  ITEMS_IS_LOADING,
  FETCH_DATA_SUCCESS,
  INITIAL_STATE,
  ADD_LIKE,
  ADD_QUESTION,
  FETCH_EXPERTS_DATA_SUCCESS,
  // QUESTION_CONTENT,
  QUESTION_ADDRESSED_TO,
  // QUESTION_AUTHOR,
  SORT_LATEST,
  SORT_OLDEST,
  SORT_BEST,
  SORT_WORST,
  CLEAR_EXPERTS_SELECTION
} from "../actions/actionTypes";
import { combineReducers } from "redux";

export default combineReducers({
  itemsHasErrored,
  itemsIsLoading,
  usersPosts,
  initialState,
  expertsData,
  // newQuestion,
  userSelections
});

function initialState(state = "", action) {
  switch (action.type) {
    case INITIAL_STATE:
      return action.test;
    default:
      return state;
  }
}

function itemsHasErrored(state = false, action) {
  switch (action.type) {
    case ITEMS_HAS_ERRORED:
      return action.hasErrored;
    default:
      return state;
  }
}
function itemsIsLoading(state = false, action) {
  switch (action.type) {
    case ITEMS_IS_LOADING:
      return action.isLoading;
    default:
      return state;
  }
}

export function usersPosts(state = [], action) {
  switch (action.type) {
    case FETCH_DATA_SUCCESS:
      return action.data;
    case ADD_LIKE:
      // const posts = [...state].map(post => {
      //   if (post.id === action.id) {
      //     if (!post.liked) {
      //       post.likes += 1;
      //       post.liked = true;
      //     } else {
      //       post.likes -= 1;
      //       post.liked = false;
      //     }
      //   }
      //   return post;
      const newState = [...state].map(post => {
        if (post.id === action.id) {
          if (!post.likes.includes(action.cookieID)) {
            post.likes.push(action.cookieID);
          } else {
            post.likes.splice(post.likes.indexOf(action.cookieID), 1);
          }
        }
        return post;
      });
      console.log(newState);
      return newState;
    case ADD_QUESTION:
      const stateCopy = [...state];
      stateCopy.push(action.newQuestion);
      return (state = stateCopy);
    case SORT_LATEST:
      return [...state].sort((a, b) => b.timestamp - a.timestamp);
    case SORT_OLDEST:
      return [...state].sort((a, b) => a.timestamp - b.timestamp);
    case SORT_BEST:
      return [...state].sort((a, b) => b.likes - a.likes);
    case SORT_WORST:
      return [...state].sort((a, b) => a.likes - b.likes);
    default:
      return state;
  }
}

export function expertsData(state = [], action) {
  switch (action.type) {
    case FETCH_EXPERTS_DATA_SUCCESS:
      return action.data;
    default:
      return state;
  }
}

export function userSelections(state = {}, action) {
  switch (action.type) {
    case QUESTION_ADDRESSED_TO:
      const newData = action.data;
      const updateAddressedTo = state.hasOwnProperty("questionAddressedTo")
        ? [...state.questionAddressedTo]
        : [];
      if (!updateAddressedTo.includes(newData)) {
        updateAddressedTo.push(newData);
      } else {
        updateAddressedTo.splice(updateAddressedTo.indexOf(newData), 1);
      }
      return { ...state, questionAddressedTo: updateAddressedTo };
    case CLEAR_EXPERTS_SELECTION:
      return (state = {});
    default:
      return state;
  }
}

// == The intention was to have single newQuestion reducer for both questionInput and expertsSelection, leaving it as temp maybe for future //

// export function newQuestion(state = {}, action) {
//   switch (action.type) {
//     case QUESTION_CONTENT:
//       return (state.question = action.data);
//     case QUESTION_ADDRESSED_TO:
//       const newData = action.data;
//       const updateAddressedTo = state.hasOwnProperty("questionAddressedTo")
//         ? [...state.questionAddressedTo]
//         : [];
//       if (!updateAddressedTo.includes(newData)) {
//         updateAddressedTo.push(newData);
//       } else {
//         updateAddressedTo.splice(updateAddressedTo.indexOf(newData), 1);
//       }
//       return { ...state, questionAddressedTo: updateAddressedTo };
//     case QUESTION_AUTHOR:
//       return (state.author = action.data);
//     default:
//       return state;
//   }
// }
