import React, { Component } from "react";
import { connect } from "react-redux";
import store from "../../store/store";
import { addLike } from "../../actions/actions";
import { getCookie } from "../../services/cookieIdService";

import Like from "../Like/Like";

import "./QuestionScore.css";

class QuestionScore extends Component {
  toogleLike = () => {
    store.dispatch(addLike(this.props.id, getCookie()));
  };

  render() {
    return (
      <div className="questionScore">
        <span className="questionScore__value">{this.props.likes.length}</span>
        <Like
          onClickHandler={this.toogleLike}
          id={this.props.id}
          active={this.props.likes.includes(getCookie())}
        />
      </div>
    );
  }
}

export default QuestionScore;
