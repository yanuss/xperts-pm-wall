import React from "react";
import ReactDOM from "react-dom";
import Home from "./Home";
import renderer from "react-test-renderer";
import ChannelInfo from "./ChannelInfo";

describe("test", () => {
  it("ktory dziala", () => {
    const component = renderer.create(<ChannelInfo />);
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
