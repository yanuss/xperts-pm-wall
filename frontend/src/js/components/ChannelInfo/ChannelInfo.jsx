import React, { Component } from 'react';
import './ChannelInfo.css';

class ChannelInfo extends Component{
   render(){
        return (
            <div className="header__channel-info">
              <div className="header__channel-info--heading">
                <h2>Big Data - Big Problem! Nowe wyzwania dla ludzkości!</h2>
              </div>
              <div className="header__channel-info--hashtag">
                <p>Kraków, 27 luty, #eventcode</p>
              </div>
            </div>
        )
    }
}

export default ChannelInfo;
