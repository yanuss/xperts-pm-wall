import React from "react";
import { connect } from "react-redux";
import store from "../../store/store";

import QuestionInput from "../QuestionInput/QuestionInput";
import Header from "../Header/Header";
import Menu from "../Menu/Menu";
import QuestionsList from "../QuestionsList/QuestionsList";
import ExpertsBox from "../Experts/ExpertsSection/ExpertsSection";

import "./Home.css";

const Home = props => {
  const { test, usersPosts } = props;
  return (
    <div className="container">
      <Header />
      <Menu />
      <ExpertsBox />
      <QuestionInput />
      <QuestionsList questions={usersPosts} />
    </div>
  );
};

const mapStateToProps = state => {
  return {
    usersPosts: state.usersPosts
  };
};

export default connect(mapStateToProps)(Home);
