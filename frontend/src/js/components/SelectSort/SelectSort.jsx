import React, { Component } from "react";
import "./SelectSort.css";
import OptionSort from "../OptionSort/OptionSort";

class SelectSort extends Component {
  render() {
    return (
      <select
        onChange={this.props.sort}
        value={this.props.sortedType}
        className="sort"
      >
        {this.props.options &&
          this.props.options.map(option => (
            <OptionSort
              value={option.value}
              name={option.name}
              key={Math.random()}
            />
          ))}
      </select>
    );
  }
}

export default SelectSort;
