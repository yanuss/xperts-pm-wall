import React from "react";
import PropTypes from "prop-types";
// import store from "../../store/store";
// import { connect } from "react-redux";

import "./Badge.css";

const Badge = props => {
  return <span className="badge">{props.count}</span>;
};

Badge.propTypes = {
  count: PropTypes.number
};

export default Badge;

// const mapStateToProps = state => {
//   return {
//     count: state.usersPosts.length
//   };
// };

// export default connect(mapStateToProps)(Badge);
