import React, { Component } from "react";
import "./User.css";

class User extends React.Component {
  render() {
    return <p className="user__name">{this.props.name}</p>;
  }
}

export default User;
