import React from "react";
import { Link } from "react-router-dom";
import "./NotFound.css";

const NotFound = () => (
  <div class="notfound">
    <div class="box">
      <h1>Error 404 </h1>
      <h3>Page not found</h3>
      <Link class="link" to="/">
        Return to Home Page
      </Link>
    </div>
  </div>
);

export default NotFound;
