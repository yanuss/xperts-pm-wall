import React, { Component } from "react";
import User from "../User/User";
import "./Question.css";
import QuestionScore from "../QuestionScore/QuestionScore";
import QuestionTimestamp from "../QuestionTimestamp/QuestionTimestamp";
import { getCookie } from "../../services/cookieIdService";

import { addComa } from "../../helpers/helpers";

class Question extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const userQuestion =
      this.props.question.cookieId === getCookie()
        ? "question userQuestion"
        : "question";
    return (
      <div className={userQuestion}>
        <div className="question__body">
          <h3>{this.props.question.questionAddressedTo}</h3>
          <p>{this.props.question.question}</p>
        </div>
        <div className="question__info">
          <div className="question__input">
            <User name={this.props.question.first_name} />
            <QuestionTimestamp timestamp={this.props.question.timestamp} />
          </div>
          <div className="question__score">
            <QuestionScore
              id={this.props.question.id}
              likes={this.props.question.likes}
            />
          </div>
        </div>
      </div>
    );
  }
}
export default Question;
