import React from "react";
import "./OptionSort.css";

const OptionSort = props => (
  <option className="sort-item" value={props.value}>
    {props.name}
  </option>
);

export default OptionSort;
