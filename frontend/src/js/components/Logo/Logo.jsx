import React, {Component} from 'react';
import "./Logo.css";

class Logo extends Component{
    render(){
        return (
            <div className="header__logo">
               <h2>Logo Xperts PM</h2>
               <hr/>
            </div>
        )
    }
}

export default Logo;
