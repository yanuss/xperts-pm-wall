import React from "react";

import "./DefaultButton.css";

const DefaultButton = props => {
  return (
    <button type="button" className="btn" onClick={props.handleClick}>
      {/* {props.children} */}
      {props.content}
    </button>
  );
};

export default DefaultButton;
