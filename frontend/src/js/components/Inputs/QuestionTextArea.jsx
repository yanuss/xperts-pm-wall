import React from "react";
import "./QuestionTextArea.css";

const QuestionTextArea = props => {
  return (
    <div className="input__wrap">
      <textarea
        name={props.name}
        type="text"
        placeholder={props.placeholder}
        value={props.value}
        onChange={props.handleChange}
        maxLength="360"
      />
      <span className="focus-input" />
      <span className="input-length">{props.remainingChars}</span>
      {props.children}
    </div>
  );
};

export default QuestionTextArea;
