import React from "react";
import "./DefaultInput.css";

const DefaultInput = props => {
  return (
    <div className="input__wrap">
      <input
        name={props.name}
        type="text"
        placeholder={props.placeholder}
        value={props.value}
        onChange={props.handleChange}
      />
      <span className="focus-input" />
      {props.children}
    </div>
  );
};

export default DefaultInput;
