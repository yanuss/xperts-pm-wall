import React, { Component } from "react";
import FlipMove from "react-flip-move";
import Question from "../Question/Question";
import SelectSort from "../SelectSort/SelectSort";
import store from "../../store/store";
import {
  sortLatest,
  sortOldest,
  sortBest,
  sortWorst
} from "../../actions/sortingPosts";

import "./QuestionsList.css";

class QuestionsList extends Component {
  state = {
    questions: this.props.questions,
    sortedType: "best",
    options: [
      { name: "Najwyzej oceniane", value: "best" },
      { name: "Najmniej oceniane", value: "worst" },
      { name: "Najnowsze", value: "new" },
      { name: "Najstarsze", value: "old" }
    ]
  };

  componentDidMount() {
    this.sort(this.state.sortedType);
  }

  sortLatest = () => {
    store.dispatch(sortLatest());
  };

  sortOldest = () => {
    store.dispatch(sortOldest());
  };

  sortBest = () => {
    store.dispatch(sortBest());
  };

  sortWorst = () => {
    store.dispatch(sortWorst());
  };

  sort = e => {
    const data = this.state.questions;
    const type = typeof e === "string" ? e : e.target.value;

    let sortedQuestions = [];
    switch (type) {
      case "new":
        sortedQuestions = this.sortLatest(data);
        break;
      case "old":
        sortedQuestions = this.sortOldest(data);
        break;
      case "best":
        sortedQuestions = this.sortBest(data);
        break;
      case "worst":
        sortedQuestions = this.sortWorst(data);
        break;
      default:
        sortedQuestions = this.sortLatest(data);
    }

    this.setState({ questions: sortedQuestions, sortedType: type });
  };

  render() {
    const questionsList = this.props.questions.map(question => (
      <Question question={question} key={question.id} />
    ));
    return (
      <section className="questions-wall">
        <>
          <div className="outer-wrapper">
            <div className="outer-label">Pytania</div>
            {this.props.questions.length > 0 && (
              <SelectSort
                sort={this.sort}
                sortedType={this.state.sortedType}
                options={this.state.options}
              />
            )}
          </div>
          <FlipMove>
            {questionsList.length > 0 ? (
              questionsList
            ) : (
              <div className="outer-wrapper">
                <p className="outer-label outer-label-questions">
                  Nie ma jeszcze zadnych pytan
                </p>
              </div>
            )}
          </FlipMove>
        </>
      </section>
    );
  }
}

export default QuestionsList;
