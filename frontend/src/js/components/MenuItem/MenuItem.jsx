import React, { Component } from "react";
import "./MenuItem.css";

const MenuItem = props => {
  return (
    <a
      className={props.active ? "menu__item active" : "menu__item"}
      onClick={props.handleClick}
    >
      {props.name}
      {props.children > 0 && props.children}
    </a>
  );
};

export default MenuItem;
