import React from "react";
import "./Like.css";

const Like = props => (
  <span className="like-wrapper" onClick={props.onClickHandler}>
    <svg
      className="like"
      width="25px"
      height="22px"
      version="1.0"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 67.000000 62.000000"
      preserveAspectRatio="xMidYMid meet"
    >
      <g
        id={props.id}
        transform="translate(0.000000,62.000000) scale(0.100000,-0.100000)"
        fill="grey"
        stroke="none"
        className={props.active ? "active" : ""}
      >
        <path
          d="M297 563 c-4 -3 -7 -37 -7 -74 0 -65 -1 -69 -35 -97 l-36 -29 3 -144
3 -144 138 -3 c177 -3 176 -4 201 157 17 112 17 115 -2 140 -17 23 -27 26 -91
29 l-71 3 0 60 c0 50 -4 64 -25 84 -23 24 -64 33 -78 18z"
        />
        <path
          d="M93 373 c-10 -3 -13 -41 -13 -138 0 -123 2 -135 19 -145 22 -11 61
-13 85 -4 14 5 16 28 16 150 l0 144 -47 -1 c-27 0 -54 -3 -60 -6z"
        />
      </g>
    </svg>
  </span>
);

export default Like;
