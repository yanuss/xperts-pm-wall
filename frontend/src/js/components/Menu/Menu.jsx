import React, { Component } from "react";
import PropTypes from "prop-types";
import store from "../../store/store";
import { connect } from "react-redux";

import MenuItem from "../MenuItem/MenuItem";
import Badge from "../Badge/Badge";

import "./Menu.css";

class Menu extends Component {
  state = {
    active: "Zapytaj eksperta"
  };

  toggleActive = e => {
    console.log(e.target.textContent);
    this.setState({
      active: e.target.textContent.replace(/[0-9]/g, "")
    });
  };

  render() {
    return (
      <nav className="menu">
        <MenuItem
          name={"Zapytaj eksperta"}
          handleClick={this.toggleActive}
          active={this.state.active === "Zapytaj eksperta" ? true : false}
        >
          {this.props.usersPostsCount && (
            <Badge count={this.props.usersPostsCount} />
          )}
        </MenuItem>

        <MenuItem
          name={"Komentuj i odpowiadaj"}
          handleClick={this.toggleActive}
          active={this.state.active === "Komentuj i odpowiadaj" ? true : false}
        >
          {this.props.comments && <Badge count={this.props.usersPostsCount} />}
        </MenuItem>
      </nav>
    );
  }
}

const mapStateToProps = state => {
  return {
    usersPostsCount: state.usersPosts.length
  };
};

export default connect(mapStateToProps)(Menu);
