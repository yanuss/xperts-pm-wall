import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import store from "../../../store/store";

import ExpertsBox from "../ExpertsBox/ExpertsBox";

import "./ExpertsSection.css";

class ExpertsSection extends Component {
  render() {
    const { experts } = this.props;
    return (
      <div className="experts__section">
        <h3>
          Mozesz wybrać jednego lub kilku ekspertów, ale takze zadać pytanie
          ogólne bez wskazania któregokolwiek z nich
        </h3>
        <div className="experts__box-wrapper">
          {experts &&
            experts.map(expert => <ExpertsBox key={expert.id} {...expert} />)}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    experts: state.expertsData
  };
};

ExpertsSection.propTypes = {};

export default connect(mapStateToProps)(ExpertsSection);
