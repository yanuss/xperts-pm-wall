import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import store from "../../../store/store";
import { questionAddressedTo } from "../../../actions/newQuestion";

import ExpertsAvatar from "../ExpertsAvatar/ExpertsAvatar";

import "./ExpertsBox.css";

class ExpertsBox extends Component {
  handleClick = () => {
    store.dispatch(
      questionAddressedTo(`${this.props.first_name} ${this.props.last_name}`)
    );
  };

  render() {
    const { first_name, last_name, avatar } = this.props;
    const expertName = this.props.first_name + " " + this.props.last_name;
    const classSwitch = this.props.questionAddressedTo
      ? this.props.questionAddressedTo.includes(expertName)
      : "";
    return (
      <div
        className={classSwitch ? "expert__box active" : "expert__box "}
        onClick={this.handleClick}
      >
        <ExpertsAvatar avatar={avatar} />
        <div className="expert__box--text">
          {first_name}
          <br />
          {last_name}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    questionAddressedTo: state.userSelections.questionAddressedTo
  };
};

ExpertsBox.propTypes = {};

export default connect(mapStateToProps)(ExpertsBox);
