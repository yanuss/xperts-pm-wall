import React from "react";
import PropTypes from "prop-types";

import "./ExpertsAvatar.css";

const ExpertsAvatar = props => {
  return (
    <div className="experts__avatar">
      <img src={`${props.avatar}`} />
    </div>
  );
};

ExpertsAvatar.propTypes = {};

export default ExpertsAvatar;
