import React, { Component } from "react";
import "./QuestionTimestamp.css";

class QuestionTimestamp extends Component {
  createTimestamp = timestamp => {
    let timeHour = new Date(timestamp).toLocaleTimeString([], {
      hour: "2-digit",
      minute: "2-digit"
    });
    return timeHour !== "Invalid Date" ? timeHour : "";
  };

  render() {
    return (
      <p className="question-timestamp">
        {this.createTimestamp(this.props.timestamp)}
      </p>
    );
  }
}
export default QuestionTimestamp;
