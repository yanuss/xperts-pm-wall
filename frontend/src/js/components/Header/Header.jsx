import React, { Component } from "react";
import "./Header.css";
import Logo from "../Logo/Logo";
import ChannelInfo from "../ChannelInfo/ChannelInfo";

class Header extends Component {
  render() {
    return (
      <header className="header">
        <Logo />

        <ChannelInfo />

      </header>
    );
  }
}

export default Header;
