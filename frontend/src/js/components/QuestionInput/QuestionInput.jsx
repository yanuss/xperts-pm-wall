import React, { Component } from "react";
import { connect } from "react-redux";
import store from "../../store/store";
import { addQuestion } from "../../actions/actions";
import { postPostsData } from "../../actions/actions";
import { clearExpertsSelection } from "../../actions/newQuestion";
import DefaultButton from "../Buttons/DefaultButton";
import DefaultInput from "../Inputs/DefaultInput";
import QuestionTextArea from "../Inputs/QuestionTextArea";
import { getCookie } from "../../services/cookieIdService";

import "./QuestionInput.css";
class QuestionInput extends Component {
  state = { questionContent: "", questionAuthor: "", remainingChars: 360 };

  questionLabelContent = (selections, expertsCount) => {
    if (!selections || selections.length === 0) {
      return "Ogólne";
    } else if (selections.length === expertsCount) {
      return "Do wszystkich";
    } else {
      return "Do " + selections.join(", ");
    }
  };

  handleChange = event => {
    const remainingChars = 360 - event.target.value.length;
    const name = event.target.name;
    this.setState({ [name]: event.target.value, remainingChars });
  };
  handleSubmit = event => {
    event.preventDefault();
    store.dispatch(
      postPostsData({
        id: Math.floor(Math.random() * 1000) + 100,
        questionAddressedTo: this.questionLabelContent(
          this.props.userSelections.questionAddressedTo,
          this.props.experts.length
        ),
        first_name: this.state.questionAuthor,
        question: this.state.questionContent,
        likes: [getCookie()],
        timestamp: new Date().getTime(),
        cookieId: getCookie()
      })
    );
    store.dispatch(clearExpertsSelection());
    this.setState({
      questionContent: "",
      questionAuthor: "",
      remainingChars: 360
    });
  };

  render() {
    const { questionTo, userSelections, experts } = this.props;
    return (
      <div className="questions-wall">
        <div className="outer-wrapper outer-label">Twoje pytanie</div>
        <form className="question question__form" onSubmit={this.handleSubmit}>
          <label className="question__label">
            {this.questionLabelContent(
              userSelections.questionAddressedTo,
              experts.length
            )}
          </label>
          <QuestionTextArea
            name="questionContent"
            handleChange={this.handleChange}
            value={this.state.questionContent}
            placeholder="Wpisz swoje pytanie..."
            remainingChars={this.state.remainingChars}
          />
          <div className="center">
            <DefaultInput
              name="questionAuthor"
              handleChange={this.handleChange}
              value={this.state.questionAuthor}
              placeholder="Twoje imie"
            >
              <DefaultButton content="WYŚLIJ" handleClick={this.handleSubmit} />
            </DefaultInput>
          </div>
        </form>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    userSelections: state.userSelections,
    experts: state.expertsData
  };
};

export default connect(mapStateToProps)(QuestionInput);
