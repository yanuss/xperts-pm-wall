import {
  QUESTION_CONTENT,
  QUESTION_ADDRESSED_TO,
  QUESTION_AUTHOR,
  CLEAR_EXPERTS_SELECTION
} from "./actionTypes.js";

//not in use yet, maybe never will be
export function questionContent(data) {
  return {
    type: QUESTION_CONTENT,
    data
  };
}

// in use !
export function questionAddressedTo(data) {
  return {
    type: QUESTION_ADDRESSED_TO,
    data
  };
}

export function clearExpertsSelection() {
  return {
    type: CLEAR_EXPERTS_SELECTION
  };
}

//not in use yet, maybe never will be
export function questionAuthor(data) {
  return {
    type: QUESTION_AUTHOR,
    data
  };
}
