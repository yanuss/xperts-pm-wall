import { SORT_LATEST, SORT_OLDEST, SORT_BEST, SORT_WORST } from "./actionTypes";

export function sortLatest() {
  return {
    type: SORT_LATEST
  };
}
export function sortOldest() {
  return {
    type: SORT_OLDEST
  };
}
export function sortBest() {
  return {
    type: SORT_BEST
  };
}
export function sortWorst() {
  return {
    type: SORT_WORST
  };
}
