import {
  DEFAULT_TEST,
  GET_POSTS,
  ITEMS_HAS_ERRORED,
  ITEMS_IS_LOADING,
  FETCH_DATA_SUCCESS,
  INITIAL_STATE,
  ADD_LIKE,
  ADD_QUESTION,
  FETCH_EXPERTS_DATA_SUCCESS
} from "./actionTypes.js";
import axios from "axios";

export const defaultTest = value => ({
  type: DEFAULT_TEST,
  value: value
});

export function initialState(value) {
  return {
    type: INITIAL_STATE,
    test: value
  };
}

export function itemsHasErrored(bool) {
  return {
    type: ITEMS_HAS_ERRORED,
    hasErrored: bool
  };
}
export function itemsIsLoading(bool) {
  return {
    type: ITEMS_IS_LOADING,
    isLoading: bool
  };
}

export function usersPosts(data) {
  return {
    type: FETCH_DATA_SUCCESS,
    data
  };
}

export function getExperts(data) {
  return {
    type: FETCH_EXPERTS_DATA_SUCCESS,
    data
  };
}

export function addLike(id, cookieID) {
  return {
    type: ADD_LIKE,
    id,
    cookieID
  };
}

export function addQuestion(newQuestion) {
  return {
    type: ADD_QUESTION,
    newQuestion
  };
}

export function getPostsData(url) {
  return dispatch => {
    dispatch(itemsIsLoading(true));
    axios
      .get(`${url}`)
      .then(response => {
        if (response.statusText !== "OK") {
          throw Error(response.statusText);
        }
        dispatch(itemsIsLoading(false));
        return response;
      })
      .then(response => response.data)
      .then(items => {
        dispatch(usersPosts(items));
      })
      .catch(() => {
        dispatch(itemsHasErrored(true));
        dispatch(itemsIsLoading(false));
      });
  };
}

export function getExpertsData(url) {
  return dispatch => {
    dispatch(itemsIsLoading(true));
    axios
      .get(`${url}`)
      .then(response => {
        if (response.statusText !== "OK") {
          throw Error(response.statusText);
        }
        dispatch(itemsIsLoading(false));
        return response;
      })
      .then(response => response.data)
      .then(items => {
        dispatch(getExperts(items));
      })
      .catch(() => {
        dispatch(itemsHasErrored(true));
        dispatch(itemsIsLoading(false));
      });
  };
}
export function postPostsData(post) {
  return dispatch => {
    dispatch(itemsIsLoading(true));
    dispatch(addQuestion(post));
    axios.post("http://localhost:3000/posts", post).then(response => {
      // if (response.statusText !== "OK") {
      //   throw Error(response.statusText);
      // }
      return response;
    });
  };
}
