export const setCookie = () => {
  const random = Math.floor(Math.random() * 100000 + 100000);
  localStorage.setItem("cookieId", JSON.stringify(random));
};

export const getCookie = () => {
  const random = localStorage.getItem("cookieId");
  if (random) {
    return JSON.parse(random);
  }
  return false;
};

export const checkIfCookieExists = () => {
  if (!getCookie()) {
    return true;
  }
  return false;
};
