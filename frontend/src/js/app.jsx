import React, { Component } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "./components/Home/Home";
import NotFound from "./components/NotFound/NotFound";
import { Provider } from "react-redux";
import store from "./store/store";
import "../css/app.css";

import {
  initialState,
  getPostsData,
  getExpertsData
} from "../js/actions/actions";
import { setCookie, checkIfCookieExists } from "./services/cookieIdService";
class App extends Component {
  componentDidMount() {
    store.dispatch(initialState("redux dziala"));
    store.dispatch(getPostsData("http://localhost:3000/posts"));
    store.dispatch(getExpertsData("http://localhost:3000/experts"));
    if (checkIfCookieExists()) {
      setCookie();
    }
  }
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route component={NotFound} />
          </Switch>
        </Router>
      </Provider>
    );
  }
}
ReactDOM.render(<App />, document.querySelector("#app"));
